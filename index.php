<?php

error_reporting(E_ALL);

define('ROOT', dirname(__FILE__));

include_once ROOT . '/lib/functions.php';
$way_view = ROOT .'/templates/authForm.html';
$way_txt =  ROOT . '/records.txt';

$array_placeholder = array('{TITLE}' => 'Blog',
                           '{CONTENT}' => '',
                           '{CLASSNAME_REG}' => '',
                          '{CLASSNAME_LOGIN}' => '',
                          '{ACTION}'=>'',
                          '{FIELDNAME}'=>'',
                            '{HIDE}' => '<!--',
                          '{ENDHIDE}'=> '-->',
                          '{ERROR}' => '',
                          '{MASSAGE}'=> '',
                          '{EXIT}' => '<!--',
                          '{ENDEXIT}'=> '-->',);

if($_SERVER['REQUEST_URI']=='/' || $_SERVER['REQUEST_URI']=='/reg' || $_SERVER['REQUEST_URI']=='/login'){
    
    if($_SERVER['REQUEST_URI']=='/'){
        
    }    
    
    if($_SERVER['REQUEST_URI']=='/reg'){
        $array_placeholder['{CLASSNAME_REG}'] = 'show';
        $array_placeholder['{CLASSNAME_LOGIN}'] = 'hide';
        $array_placeholder['{ACTION}'] = 'Registration';
        $array_placeholder['{FIELDNAME}'] = 'reg';
        $array_placeholder['{HIDE}'] = '';
        $array_placeholder['{ENDHIDE}'] = '';
        
        if(!empty($_POST['send'])){
            $errors = array();
            $errors = validation_errors($_POST['reg'], $_POST['password']);
            if($errors){
                foreach($errors as $value){
                    $array_placeholder['{ERROR}'] = $value;
                }
            }
            
            $write = false;
            
            if(!$errors && filesize($way_txt)){

                $read_data = get_file($way_txt);
                $arr = array_column($read_data, 'reg');

                $key = array_search($_POST['reg'], $arr);

                if($arr[$key] == $_POST['reg']){
                    $array_placeholder['{MASSAGE}'] = 'This is login busy!';
                    $write = false;
                }
                else{
                    $write = true;
                }
            }
            else{
                $write = true;
            }
            
            if(!$errors && $write){
                
                    $errors = set_in_file($way_txt, $_POST);
                    
                    try{
                        
                    }
                    catch (Exception $e) { 
                        $message = $e->getMessage(); 
                        echo 'Write error: '.$message; 
                    }
                
                    if($errors){
                        $array_placeholder['{MASSAGE}'] = 'You were logged successfully!';
                        $array_placeholder['{CLASSNAME_REG}'] = 'hide';
                        $array_placeholder['{CLASSNAME_LOGIN}'] = 'hide';
                        $array_placeholder['{HIDE}'] = '<!--';
                        $array_placeholder['{ENDHIDE}'] = '-->';
                        $array_placeholder['{EXIT}'] = '';
                        $array_placeholder['{ENDEXIT}'] = '';
                    }
                    else{
                        foreach($errors as $value){
                        $array_placeholder['{ERROR}'] = $value;
                    }
                    }
            }          
        }
    }
    
    
    if($_SERVER['REQUEST_URI']=='/login'){
        $array_placeholder['{CLASSNAME_REG}'] = 'hide';
        $array_placeholder['{CLASSNAME_LOGIN}'] = 'show';
        $array_placeholder['{ACTION}'] = 'Log in';
        $array_placeholder['{FIELDNAME}'] = 'login';
        $array_placeholder['{HIDE}'] = '';
        $array_placeholder['{ENDHIDE}'] = '';
        
        if(!empty($_POST['send'])){
            $errors = validation_errors($_POST['login'], $_POST['password']);
            if($errors){
                foreach($errors as $value){
                    $array_placeholder['{ERROR}'] = $value;
                }
            }
            
        if(!$errors){
            
            $read_data = get_file($way_txt);
            $login = array_column($read_data, 'reg');
            $password = array_column($read_data, 'password');
            $key_login = array_search($_POST['login'], $login);
            $key_password = array_search($_POST['password'], $password);
        
            
            if($login[$key_login] == $_POST['login'] && $password[$key_password] == $_POST['password']){
                    
                    session_start();
                    $array_placeholder['{MASSAGE}'] = 'Hello, ' . $_POST['login'];
                    $array_placeholder['{CLASSNAME_REG}'] = 'hide';
                    $array_placeholder['{CLASSNAME_LOGIN}'] = 'hide';
                    $array_placeholder['{HIDE}'] = '<!--';
                    $array_placeholder['{ENDHIDE}'] = '-->';
                    $array_placeholder['{EXIT}'] = '';
                    $array_placeholder['{ENDEXIT}'] = '';
            }
            else{
                     $array_placeholder['{MASSAGE}'] = 'Error!';
            }
        }
        
        
        }
    }
}

else{
    header("HTTP/1.1 404 Not Found");
    exit(file_get_contents('./404.html'));
}
    

echo placeholder($way_view, $array_placeholder);
