<?php

function placeholder($way, $array){
    $content = file_get_contents($way);
    
    foreach($array as $key => $value){
        $pattern = '/' . $key . '/';
        $content = preg_replace($pattern, $value, $content);
    }
    return $content;
}

function validation_errors($name, $pass){
    $error = array(); 
    if(!empty($name)){
        $error = '';
    }
    if(empty($name) && empty($pass)){
        $error[] = 'Enter your username and password!';
    }
    elseif(empty($name)){
        $error[] = 'Enter your username!';
    }
    elseif(empty($pass)){
        $error[] = 'Enter your password!';
    }
    return $error;
}

function set_in_file($way, $post){
    $error = array(); 
    if($post){
        if(file_put_contents($way, serialize($post) . '\n', FILE_APPEND)){
            $error[] = '';
        }
        else{
            $error[] = 'Not registered';
            throw new Exception('Not registered');
        }
    }
    return $error;
}

function get_file($way){
    $result = array();
    if(file_exists($way)){
        $data = explode('\n', file_get_contents($way));
        foreach($data as $key => $value){
            $result[] = unserialize($value);
        }
        
        
        return($result);
    }
}
